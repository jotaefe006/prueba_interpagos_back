<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookRequest;
use App\Http\Requests\BookUpdateRequest;
use Illuminate\Http\Request;
use App\Book;


class BookController extends Controller
{
    /**
     * GET Listar registros
     */
    public function index()
    {
        $books = Book::get();
        return $books;
    }

    /**
     * Post Insertar datos
     */
    public function store(BookRequest $request)
    {
        $input = $request->all();

        Book::create($input);

        return response()->json([
            'res' => true,
            'message' => 'Registro exitoso'
        ], 200);
    }

    /**
     * GET Obtiene un registro
     */
    public function show(Book $book)
    {
        return $book;
    }

    /**
     * PUT actualiza un registro
     */
    public function update(BookUpdateRequest $request, Book $book)
    {
        $input = $request->all();
        $book->update($input);

        return response()->json([
            'res' => true,
            'message' => 'Registro actualizado'
        ], 200);
    }

    /**
     * DELETE Elimina un registro
     */
    public function destroy($id)
    {
        Book::destroy($id);

        return response()->json([
            'res' => true,
            'message' => 'Registro eliminado'
        ], 200);
    }
}
